import React, { ReactElement } from "react";
import { CustomHeader } from "../components/Header/CustomHeader";
import Mainlayout from "../components/Layout/MainLayout";

const About = () => {
  return (
    <>
      <CustomHeader>
        <title>Giới thiệu</title>
      </CustomHeader>
      <div className="bg-slate-600 mt-10 rounded-xl ">
        <div className="md:px-10 md:py-20 flex p-2 md:p-0 md:flex-row flex-col-reverse">
          <div className="md:w-1/2 text-white md:pr-8">
            <div className="text-xl md:text-5xl mt-2 md:mt-0 font-bold flex uppercase">
              Giới thiệu <p className="text-white pl-2 md:pl-5"> Nhóm 15</p>
            </div>
            <div className="mt-5 text-lg">
              <p>
                Nhóm 15 của chúng mình bao gồm 6 thành viên : Đặng Minh Quân, Vũ
                Hải Quân, Nguyễn Hữu Quân, Trần Duy Phong, Thái Ngọc Sơn và Phan
                Xuân Tài.
              </p>{" "}
              <br />
              <p>
                Nhóm được thành lập và giúp đỡ nhau trong quá trình học quân sự
                từ lúc năm 1. Nhóm được tạo ra với mục đích để giúp đỡ nhau
                trong học tập và có những phút giây chơi game cùng nhau sau
                những giờ học căng thẳng.
              </p>
              <br />
              <p>
                Các thành viên luôn tuân thủ những quy định mà nhóm đã đề ra,
                luôn hăng hái hoạt động tạo ra những trò chơi, những kinh nghiệm
                trong cuộc sống…Sau mỗi buổi học sẽ có những bài tập riêng biệt,
                các thành viên nhóm luôn hỗ trợ nhau hoàn thiện bài, đưa ra
                những phương pháp học và làm bài để đạt được hiệu quả cao.
              </p>
              <br />
              <p>
                Mỗi cuối tuần khi có thời gian rảnh các thành viên sẽ họp lại 1
                buổi để có những phút giây thư giãn, vui đùa bên nhau. Luôn tâm
                sự, sẽ chia cũng nhau những khó khăn, áp lực trong cuộc sống dựa
                vào đó để đưa ra những góp ý giúp mỗi bạn có những thay đổi để
                hoàn thiện bản thân hơn.
              </p>
              <br />
              <p>
                Sau 3 năm nhóm được thành lập đó là thời gian không ngắn nhưng
                cũng chẳng dài mỗi thành viên luôn đưa ra những nội dung, ý
                tưởng mới để phát triển nhóm. Luôn giúp đỡ nhau trong mọi hoàn
                cảnh để mỗi bạn hoàn thiện, phát triển bản thân và có những định
                hướng mới về cột xống trong tương lai…
              </p>
            </div>
          </div>
          <div className="md:w-1/2 w-full md:pl-10">
            <img
              src="../../images/img-nhom.jpg"
              className="rounded-lg"
              alt=""
            />
          </div>
        </div>
      </div>
    </>
  );
};

About.getLayout = function getLayout(page: ReactElement) {
  return <Mainlayout>{page}</Mainlayout>;
};
export default About;
