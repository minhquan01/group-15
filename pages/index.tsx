/* eslint-disable react/no-unescaped-entities */
import React, { ReactElement } from "react";
import { CustomHeader } from "../components/Header/CustomHeader";
import Mainlayout from "../components/Layout/MainLayout";
import { BsFacebook, BsQuestionCircle } from "react-icons/bs";
import { AiOutlineInstagram } from "react-icons/ai";
import Link from "next/link";

const people = [
  {
    name: "Minh Quân",
    birth: "11/11/2001",
    imageUrl: "../../images/team/minhquan.jpg",
    facebookUrl: "https://www.facebook.com/dangminhquan2001",
    instagramUrl: "#",
  },
  {
    name: "Hải Quân",
    birth: "21/11/2001",
    imageUrl: "../../images/team/haiquan.jpg",
    facebookUrl: "https://www.facebook.com/quan.conbocap",
    instagramUrl: "#",
  },
  {
    name: "Hữu Quân",
    birth: "16/02/2001",
    imageUrl: "../../images/team/huuquan.jpg",
    facebookUrl: "https://www.facebook.com/nguyenhuuquan1622001",
    instagramUrl: "#",
  },
  {
    name: "Duy Phong",
    birth: "19/10/2001",
    imageUrl: "../../images/team/duyphong.jpg",
    facebookUrl: "https://www.facebook.com/phong191001",
    instagramUrl: "#",
  },
  {
    name: "Xuân Tài",
    birth: "11/03/2001",
    imageUrl: "../../images/team/xuantai.jpg",
    facebookUrl: "https://www.facebook.com/itsme.meou",
    instagramUrl: "#",
  },
  {
    name: "Ngọc Sơn",
    birth: "29/06/2001",
    imageUrl: "../../images/team/ngocson.jpg",
    facebookUrl: "https://www.facebook.com/son.thaingoc.1",
    instagramUrl: "#",
  },
];

const posts = [
  {
    title:
      "Có hay không bức tranh 'Họa Bì' NFT của picasso Xuân Tài đang được định giá đến 3 tỷ đô ?",

    imageUrl: "../../images/news/tai.png",
  },
  {
    title:
      "IT là vua của mọi nghề, ông vua IT Minh Quân đưa ra nhận định cứng rắn để bác bỏ phát biểu của ông Hải Quân. Ông chia sẻ với một người với kiến thức html css cơ bản có thể thu về được mức lương 3000$.......",

    imageUrl: "../../images/news/quan.png",
  },
  {
    title:
      "Cả nhóm đạt được danh hiệu công dân giỏi, khu dân cư đặt nghi vấn về thành tích này? Liệu có gian lận trong thi cử.",

    imageUrl: "../../images/news/team.png",
  },
];

const Home = () => {
  return (
    <>
      <CustomHeader>
        <title>🖕🖕</title>
      </CustomHeader>
      <div>
        <div className="my-28 bg-gradient-to-r from-gray-600 to-black pb-16 lg:relative lg:z-10 lg:pb-0 rounded-md">
          <div className="lg:mx-auto lg:grid lg:max-w-7xl lg:grid-cols-3 lg:gap-8 lg:px-8">
            <div className="relative lg:-my-8">
              <div
                aria-hidden="true"
                className="absolute inset-x-0 top-0  bg-white lg:hidden"
              />
              <div className="mx-auto max-w-md px-4 sm:max-w-3xl sm:px-6 lg:h-full lg:p-0">
                <div className="aspect-w-10 pt-10 md:pt-0 aspect-h-6 overflow-hidden rounded-xl shadow-xl sm:aspect-w-16 sm:aspect-h-7 lg:aspect-none lg:h-full">
                  <img
                    className="object-cover rounded-md lg:h-full lg:w-full"
                    src="../../images/team.jpg"
                    alt=""
                  />
                </div>
              </div>
            </div>
            <div className="mt-12 lg:col-span-2 lg:m-0 lg:pl-8">
              <div className="mx-auto max-w-md px-4 sm:max-w-2xl sm:px-6 lg:max-w-none lg:px-0 lg:py-20">
                <blockquote>
                  <div>
                    <svg
                      className="h-12 w-12 text-white opacity-25"
                      fill="currentColor"
                      viewBox="0 0 32 32"
                      aria-hidden="true"
                    >
                      <path d="M9.352 4C4.456 7.456 1 13.12 1 19.36c0 5.088 3.072 8.064 6.624 8.064 3.36 0 5.856-2.688 5.856-5.856 0-3.168-2.208-5.472-5.088-5.472-.576 0-1.344.096-1.536.192.48-3.264 3.552-7.104 6.624-9.024L9.352 4zm16.512 0c-4.8 3.456-8.256 9.12-8.256 15.36 0 5.088 3.072 8.064 6.624 8.064 3.264 0 5.856-2.688 5.856-5.856 0-3.168-2.304-5.472-5.184-5.472-.576 0-1.248.096-1.44.192.48-3.264 3.456-7.104 6.528-9.024L25.864 4z" />
                    </svg>
                    <p className="mt-6 text-2xl font-medium text-white">
                      Dành thời gian bên nhau để biết quý trọng từng giây phút
                      có nhau.
                    </p>
                  </div>
                  <footer className="mt-6">
                    <p className="text-base font-medium text-white">
                      Chào mừng
                    </p>
                    <p className="text-base font-medium text-white">
                      Đến với nhóm 15 của bọn tớ
                    </p>
                  </footer>
                </blockquote>
              </div>
            </div>
          </div>
        </div>

        <div className="mt-20">
          <div className="text-center font-bold text-2xl md:text-4xl mb-3 uppercase">
            Nhóm 15 <span className="text-black"> Core Team</span>
          </div>
          <p className="text-center text-xs md:text-base">
            Luôn chăm chỉ, tìm tòi giúp đỡ nhau trong các công việc và hoàn
            thành tốt các bài tập nhóm của thầy cô đưa ra !
          </p>
          <ul className="space-y-4 sm:grid sm:grid-cols-2 sm:gap-6 sm:space-y-0 lg:grid-cols-3 lg:gap-8 mt-4">
            {people.map((person) => (
              <li
                key={person.name}
                className="rounded-lg border shadow-lg py-10 px-6 text-center xl:px-10 xl:text-left "
              >
                <div className="space-y-6 xl:space-y-10">
                  <img
                    className="mx-auto h-40 w-40 rounded-full xl:h-56 xl:w-56 object-cover"
                    src={person.imageUrl}
                    alt=""
                  />
                  <div className="space-y-2 xl:flex xl:items-center xl:justify-between">
                    <div className="space-y-1 text-lg font-medium leading-6">
                      <h3 className="text-slate-700">{person.name}</h3>
                      <p className="text-black">{person.birth}</p>
                    </div>

                    <ul role="list" className="flex justify-center space-x-5">
                      <li>
                        <Link
                          href={person.facebookUrl}
                          className="text-blue-500 hover:text-blue-600 text-xl"
                        >
                          <span className="sr-only">Facebook</span>
                          <BsFacebook />
                        </Link>
                      </li>
                      <li>
                        <Link
                          href={person.instagramUrl}
                          className="text-[#8a3ab9] hover:text-[#7e32aa] text-xl"
                        >
                          <span className="sr-only">Instagram</span>
                          <AiOutlineInstagram />
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </>
  );
};
Home.getLayout = function getLayout(page: ReactElement) {
  return <Mainlayout>{page}</Mainlayout>;
};

export default Home;
