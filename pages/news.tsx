/* eslint-disable react/no-unescaped-entities */
import React, { ReactElement } from "react";
import { CustomHeader } from "../components/Header/CustomHeader";
import Mainlayout from "../components/Layout/MainLayout";

const news = [
  {
    imageNew: "../../images/news/car.jpg",
    title:
      "Đại gia chứng khoán Duy Phong có dấu hiệu nhảy sang sân chơi bất động sản, tương lai chứng khoán liệu ra sao?",
    author: {
      image: "../../images/team/ngocson.png",
      name: "Ngọc Sơn",
    },
    date: "28 oct 2022",
  },
  {
    imageNew: "../../images/news/cover.jpg",
    title:
      "Quả bóng vàng Thường Tín bất ngờ rơi vào tay Thái Sơn, các cuộc tranh cãi trên MXH đặt ra nghi vấn mua giải của cầu thủ này.",
    author: {
      image: "../../images/team/xuantai.jpg",
      name: "Xuân Tài",
    },
    date: "28 oct 2022",
  },
  {
    imageNew: "../../images/news/full.jpg",
    title:
      "Shipper Hải Quân chia sẻ nghề shipper có thể là vua của mọi nghề trong tương lai, với thu nhập khủng, liệu ngành IT có bị đánh đổ ?",
    author: {
      image: "../../images/team/haiquan.jpg",
      name: "Hải Quân",
    },
    date: "29 oct 2022",
  },
];

const News = () => {
  return (
    <>
      <CustomHeader>
        <title>Tin Tức</title>
      </CustomHeader>
      <div className="pb-10">
        <div className="text-center">
          <p className="text-sm font-semibold my-10">Our blog</p>
          <h1 className="text-3xl md:text-6xl font-semibold mb-5">
            Stories & ideas
          </h1>
          <p className="opacity-70 text-sm md:text-lg">
            The latest news to driver business strategy.
          </p>
        </div>
        <div className="flex justify-between flex-col md:flex-row space-y-4 md:space-y-0 md:space-x-8 items-center">
          <div className="border-black border-2 border-solid rounded-2xl w-full md:w-1/2">
            <div className="md:py-10 md:px-5 p-5 md:p-0">
              <img
                src="../../images/news/mac.jpg"
                alt=""
                className="rounded-lg shadow-lg mb-5"
              />
              <span className="font-bold text-lg md:text-2xl">
                Nhà thiết kế hàng đầu 'Đan Phượng' đồng ý thiết kế lại logo
                xiaomi với 7 tỉ VND
              </span>
              <div className="flex items-center mt-5">
                <img
                  src="../../images/team/xuantai.jpg"
                  alt=""
                  className="rounded-full object-cover h-14 w-14 border p-[2px] border-black"
                />
                <div className="pl-2">
                  <p className="font-semibold">Xuân Tài</p>
                  <p className="text-xs">29 oct 2022</p>
                </div>
              </div>
            </div>
          </div>

          {news.map((post) => (
            <div
              key={post.title}
              className="md:hidden border-black border-2 border-solid rounded-2xl w-full md:w-1/2"
            >
              <div className="md:py-10 md:px-5 p-5 md:p-0">
                <img
                  src={post.imageNew}
                  alt=""
                  className="rounded-lg shadow-lg mb-5"
                />
                <span className="font-bold text-lg md:text-2xl">
                  {post.title}
                </span>
                <div className="flex items-center mt-5">
                  <img
                    src={post.author.image}
                    alt=""
                    className="rounded-full object-cover h-14 w-14 border p-[2px] border-black"
                  />
                  <div className="pl-2">
                    <p className="font-semibold">{post.author.name}</p>
                    <p className="text-xs">{post.date}</p>
                  </div>
                </div>
              </div>
            </div>
          ))}

          <div className="hidden md:block w-1/2 flex-row space-y-5">
            {news.map((post) => (
              <div key={post.title} className="flex space-x-3 items-center">
                <img
                  src={post.imageNew}
                  alt=""
                  className="w-1/2 h-44 rounded-lg object-cover"
                />
                <div className="w-1/2">
                  <p className="font-bold text-lg mb-2">{post.title}</p>
                  <div className="flex items-center space-x-3">
                    <img
                      src={post.author.image}
                      className="rounded-full h-10 w-10 border border-black border-solid p-[1px]"
                      alt=""
                    />
                    <div>
                      <p className="font-medium text-sm">{post.author.name}</p>
                      <p className="text-xs opacity-70">{post.date}</p>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </>
  );
};

News.getLayout = function getLayout(page: ReactElement) {
  return <Mainlayout>{page}</Mainlayout>;
};

export default News;
