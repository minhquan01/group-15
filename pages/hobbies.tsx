import React, { ReactElement } from "react";
import { CustomHeader } from "../components/Header/CustomHeader";
import Mainlayout from "../components/Layout/MainLayout";

const people = [
  {
    name: "Minh Quân",
    role: "11/11/2001",
    imageUrl: "../../images/team/minhquan.jpg",
    hobbies:
      "Chơi game sau các buổi học, xem phim và tham gia vui chơi cùng các bạn bè",
  },
  {
    name: "Hải Quân",
    role: "21/11/2001",
    imageUrl: "../../images/team/haiquan.jpg",
    hobbies: "Chơi game với bạn bè, chạy shopee food.",
  },
  {
    name: "Hữu Quân",
    role: "16/02/2001",
    imageUrl: "../../images/team/huuquan.jpg",
    hobbies: "Toxic với bạn bè, đá đểu đồng đội, gáy.",
  },
  {
    name: "Ngọc Sơn",
    role: "29/06/2001",
    imageUrl: "../../images/team/ngocson.jpg",
    hobbies: "Chơi game fifa trốn các bạn không chơi lol",
  },
  {
    name: "Xuân Tài",
    role: "11/03/2001",
    imageUrl: "../../images/team/xuantai.jpg",
    hobbies: "Vẽ vời, con tim nghệ thuật, học các ngôn ngữ mới.",
  },
  {
    name: "Duy Phong",
    role: "19/10/2001",
    imageUrl: "../../images/team/duyphong.jpg",
    hobbies: "Gi gỉ gì gi cái gì cũng thích ",
  },
];
const Hobbies = () => {
  return (
    <>
      <CustomHeader>
        <title>Sở thích</title>
      </CustomHeader>
      <ul className="space-y-10 py-10 lg:grid lg:grid-cols-2 lg:items-start lg:gap-x-8 lg:gap-y-12 lg:space-y-0">
        {people.map((person) => (
          <li key={person.name}>
            <div className="flex w-full">
              <div className="w-2/5 mr-5">
                <img
                  className="rounded-lg mx-auto h-40 w-40 xl:h-56 xl:w-56 object-cover"
                  src={person.imageUrl}
                  alt=""
                />
              </div>
              <div className="w-10/12">
                <div className="space-y-4">
                  <div className="space-y-1 text-lg font-medium leading-6">
                    <h3>{person.name}</h3>
                    <p className="text-black">{person.role}</p>
                  </div>
                  <div className="text-lg">
                    Sở thích:
                    <p className="text-gray-500">{person.hobbies}</p>
                  </div>
                </div>
              </div>
            </div>
          </li>
        ))}
      </ul>
    </>
  );
};

Hobbies.getLayout = function getLayout(page: ReactElement) {
  return <Mainlayout>{page}</Mainlayout>;
};

export default Hobbies;
