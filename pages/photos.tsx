import React, { ReactElement } from "react";
import { CustomHeader } from "../components/Header/CustomHeader";
import Mainlayout from "../components/Layout/MainLayout";

const picnics = [
  {
    name: "Vui chơi",
    description: "Cùng nhau tạo các trò chơi và khuấy động không khí.",
  },
  {
    name: "Nấu nướng",
    description: "Cùng nhau chuẩn bị nguyên liệu và nấu ăn.",
  },
];

const buffets = [
  {
    name: "Tham gia các tiệc BBQ cùng nhau.",
    description:
      "Cùng nhau tổ chức các bữa tiệc BBQ ngoài trời, cùng nhau ăn uống vui vẻ.",
    imageSrc: "../../images/photos/bbq1.jpg",
  },
  {
    name: "Nấu nướng",
    description: "Phân chia nấu nướng cùng nhau hehe.",
    imageSrc: "../../images/photos/bbq2.jpg",
  },
];

const Photos = () => {
  return (
    <div className="h-screen">
      <CustomHeader>
        <title>Ảnh Nhóm</title>
      </CustomHeader>
      <div className="pt-5">
        <div className="mx-auto ">
          <div className="grid grid-cols-1 items-center gap-y-16 gap-x-8 lg:grid-cols-2">
            <div>
              <div className="border-b border-gray-200 pb-10">
                <h2 className="font-medium text-gray-500">Dã ngoại</h2>
                <p className="mt-2 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
                  Cuộc dã ngoại của bọn mình
                </p>
              </div>

              <dl className="mt-10 space-y-10">
                {picnics.map((picnic) => (
                  <div key={picnic.name}>
                    <dt className="text-sm font-medium text-gray-900">
                      {picnic.name}
                    </dt>
                    <dd className="mt-3 text-sm text-gray-500">
                      {picnic.description}
                    </dd>
                  </div>
                ))}
              </dl>
            </div>

            <div>
              <div className="aspect-w-1 aspect-h-1 overflow-hidden rounded-lg bg-gray-100">
                <img
                  src="../../images/photos/picnic7.jpg"
                  alt=""
                  className="h-full w-full object-cover object-center"
                />
              </div>
              <div className="mt-4 grid grid-cols-2 gap-4 sm:mt-6 sm:gap-6 lg:mt-8 lg:gap-8">
                <div className="aspect-w-1 aspect-h-1 overflow-hidden rounded-lg bg-gray-100">
                  <img
                    src="../../images/photos/picnic6.jpg"
                    alt=""
                    className="h-full w-full object-cover object-center"
                  />
                </div>
                <div className="aspect-w-1 aspect-h-1 overflow-hidden rounded-lg bg-gray-100">
                  <img
                    src="../../images/photos/picnic4.jpg"
                    alt=""
                    className="h-full w-full object-cover object-center"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="">
        <div className="mx-auto max-w-2xl py-24 px-4 sm:px-6  lg:max-w-7xl lg:px-8">
          <div className="mx-auto max-w-3xl text-center">
            <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
              BBQ
            </h2>
            <p className="mt-4 text-gray-500">Quây quần bên nhau.</p>
          </div>

          <div className="mt-16 space-y-16">
            {buffets.map((buffet, buffetIdx) => (
              <div
                key={buffet.name}
                className="flex flex-col-reverse lg:grid lg:grid-cols-12 lg:items-center lg:gap-x-8"
              >
                <div
                  className={`mt-6 lg:mt-0 lg:row-start-1 lg:col-span-5 xl:col-span-4 ${
                    buffetIdx % 2 === 0
                      ? "lg:col-start-1"
                      : "lg:col-start-8 xl:col-start-9"
                  }`}
                >
                  <h3 className="text-lg font-medium text-gray-900">
                    {buffet.name}
                  </h3>
                  <p className="mt-2 text-sm text-gray-500">
                    {buffet.description}
                  </p>
                </div>
                <div
                  className={`flex-auto lg:row-start-1 lg:col-span-7 xl:col-span-8 ${
                    buffetIdx % 2 === 0
                      ? "lg:col-start-6 xl:col-start-5"
                      : "lg:col-start-1"
                  }`}
                >
                  <div className="aspect-w-5 aspect-h-2 overflow-hidden rounded-lg bg-gray-100">
                    <img src={buffet.imageSrc} className="object-cover" />
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};
Photos.getLayout = function getLayout(page: ReactElement) {
  return <Mainlayout>{page}</Mainlayout>;
};

export default Photos;
