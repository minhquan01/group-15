import React from "react";
import Header from "../Header";
import BannerHeader from "../Header/Banner";

const Mainlayout = ({ children }: ChildrenProps) => {
  return (
    <div className="bg-gray-500">
      <div className=" px-5 mx-auto flex justify-center items-center h-screen">
        <div className="h-[95%] w-full rounded-lg overflow-hidden bg-slate-100 ">
          <div className="h-[11%]">
            <BannerHeader />
          </div>
          <div className="grid h-full overflow-hidden grid-cols-7 gap-x-5 pl-10 pr-5">
            <div className="col-span-1">
              <div className="fixed flex-col top-9 left-10 h-[90%] w-1/6 bg-white rounded-lg shadow-lg">
                <Header />
              </div>
            </div>
            <main className=" col-span-6">
              <div className="overflow-y-scroll max-h-[calc(100vh-7%)] ml-14 mx-auto pb-10">
                {children}
              </div>
            </main>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Mainlayout;
