import React from "react";
import { AiOutlineArrowUp } from "react-icons/ai";
import { useState, useEffect } from "react";

const BtnBackToTop = () => {
  const [scrollCurrent, setScrollCurrent] = useState(0);
  const [hiddenBtn, setHiddenBtn] = useState("opacity-0 pointer-events-none");
  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 100 && scrollCurrent > window.scrollY) {
        setHiddenBtn("opacity-100");
      } else {
        setHiddenBtn("opacity-0 pointer-events-none");
      }
      setScrollCurrent(window.scrollY);
    });
  });

  const BacktoTop = () => {
    window.scrollTo(0, 0);
  };
  return (
    <div
      className={`transition-all hover:bg-opacity-70 cursor-pointer fixed z-[9999] bottom-7 right-5 text-2xl md:text-3xl text-white bg-black bg-opacity-60 max-w-fit p-3 rounded-full ${hiddenBtn}`}
      onClick={BacktoTop}
    >
      <AiOutlineArrowUp />
    </div>
  );
};

export default BtnBackToTop;
