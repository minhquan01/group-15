import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { GrClose } from "react-icons/gr";

const NavBar = ({ openMenu, setOpenMenu }: any) => {
  const router = useRouter();
  const path = router.pathname;
  const [pathSelected, setPathSelected] = useState("/");

  useEffect(() => {
    setPathSelected(path);
  }, [path]);

  const customerNav = [
    {
      url: "/",
      label: "Trang chủ",
    },
    {
      url: "/about",
      label: "Giới thiệu",
    },
    {
      url: "/hobbies",
      label: "Sở thích",
    },
    {
      url: "/photos",
      label: "Ảnh",
    },
    {
      url: "/news",
      label: "Tin tức",
    },
  ];
  return (
    <>
      <nav className="m-0 hidden space-y-4 px-2 md:flex md:flex-col items-baseline">
        {customerNav.map((item, index) => (
          <Link href={item.url} key={index}>
            <div
              key={index}
              className={` px-10  py-1 text-lg font-medium ${
                pathSelected === item.url
                  ? "bg-black text-white rounded-lg"
                  : "text-gray-500 hover:text-black opacity-80"
              }`}
            >
              {item.label}
            </div>
          </Link>
        ))}
      </nav>
      <div
        className={`md:hidden fixed inset-0 z-[999] bg-white transition-all duration-300 ${
          openMenu ? "translate-x-[0%]" : "translate-x-full"
        }`}
      >
        <div className="p-5 w-screen h-screen ">
          <div className="w-full flex justify-end items-center">
            <div className="p-1">
              <GrClose
                className="text-2xl "
                onClick={() => {
                  setOpenMenu(false);
                }}
              />
            </div>
          </div>
          <nav className="mt-5">
            <div>
              {customerNav.map((item, index) => (
                <Link href={item.url} key={index}>
                  <p
                    key={index}
                    onClick={() => {
                      setOpenMenu(false);
                    }}
                    className={`px-2 py-1  flex items-center justify-center my-2 text-xl font-normal  ${
                      pathSelected === item.url
                        ? " bg-orange-500 rounded-full text-white"
                        : "text-black "
                    }`}
                  >
                    {item.label}
                  </p>
                </Link>
              ))}
            </div>
          </nav>
        </div>
      </div>
    </>
  );
};

export default NavBar;
