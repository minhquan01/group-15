import React from "react";
import { BsFacebook } from "react-icons/bs";
import {
  AiOutlineInstagram,
  AiOutlineTwitter,
  AiFillGithub,
} from "react-icons/ai";
import Link from "next/link";

const navigation = {
  main: [
    { name: "Trang chủ", href: "/" },
    { name: "Giới thiệu", href: "/about" },
    { name: "Sở thích", href: "/hobbies" },
    { name: "Ảnh", href: "/photos" },
    { name: "Tin tức", href: "/news" },
  ],
  social: [
    {
      name: "Facebook",
      href: "#",
      icon: <BsFacebook />,
    },
    {
      name: "Instagram",
      href: "#",
      icon: <AiOutlineInstagram />,
    },
    {
      name: "Twitter",
      href: "#",
      icon: <AiOutlineTwitter />,
    },
    {
      name: "GitHub",
      href: "#",
      icon: <AiFillGithub />,
    },
  ],
};
const Footer = () => {
  return (
    <footer className="bg-white">
      <div className="mx-auto max-w-7xl overflow-hidden py-12 px-4 sm:px-6 lg:px-8">
        <nav
          className="-mx-5 -my-2 flex flex-wrap justify-center"
          aria-label="Footer"
        >
          {navigation.main.map((item) => (
            <div key={item.name} className="px-5 py-2">
              <Link
                href={item.href}
                className="text-base text-gray-500 hover:text-gray-900"
              >
                {item.name}
              </Link>
            </div>
          ))}
        </nav>
        <div className="mt-8 flex justify-center space-x-6">
          {navigation.social.map((item) => (
            <a
              key={item.name}
              href={item.href}
              className="text-gray-400 hover:text-gray-500"
            >
              <span className="sr-only">{item.name}</span>
              <p className="text-xl">{item.icon}</p>
            </a>
          ))}
        </div>
        <p className="mt-8 text-center text-base text-gray-400">
          &copy; 2022 Group 15, Inc. All rights reserved.
        </p>
      </div>
    </footer>
  );
};

export default Footer;
