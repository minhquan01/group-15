import React from "react";

const BannerHeader = () => {
  return (
    <div className="bg-gradient-to-r from-gray-700 via-slate-800-500 to-black w-full h-full">
      <div className="ml-28 h-full w-full mx-auto flex items-center justify-center">
        <img src="/images/sv_logo_dashboard.png" alt="" />
      </div>
    </div>
  );
};

export default BannerHeader;
