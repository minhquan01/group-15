import React from "react";
import NavBar from "../NavBar";
import { BiMenu } from "react-icons/bi";
import { useState } from "react";

const Header = () => {
  const [openMenu, setOpenMenu] = useState(false);

  return (
    <div className="flex flex-col items-center justify-between h-14 md:h-16 max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
      <img
        src="../../images/logo.png"
        className="w-28 md:w-44 mt-5 mb-20"
        alt=""
      />
      <NavBar openMenu={openMenu} setOpenMenu={setOpenMenu} />
      <BiMenu
        className="text-2xl md:hidden"
        onClick={() => setOpenMenu(true)}
      />
    </div>
  );
};

export default Header;
